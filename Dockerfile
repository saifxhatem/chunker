##TODO:: FIX
# Use the official Python base image based on Alpine Linux
FROM python:3.9-alpine

# Set the working directory in the container
WORKDIR /app

# Copy the requirements.txt file to the container
COPY requirements.txt .

# Install the Python dependencies
RUN apk add --no-cache gcc musl-dev && \
    pip install --no-cache-dir -r requirements.txt && \
    apk del gcc musl-dev

# Copy the script.py file to the container
COPY src/chunker.py .

# Run the Python script when the container starts
CMD ["python", "chunker.py"]
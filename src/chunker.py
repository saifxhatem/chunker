import pandas as pd
import argparse
import sys

def chunk_excel_file(file_path, chunk_size):
    # Read the Excel file
    df = pd.read_excel(file_path)
    
    # Get the total number of rows
    total_rows = df.shape[0]
    
    # Calculate the number of chunks
    num_chunks = total_rows // chunk_size + 1
    
    # Create the chunks
    chunks = [df[i*chunk_size:(i+1)*chunk_size] for i in range(num_chunks)]
    
    # Save each chunk to a separate file
    for i, chunk in enumerate(chunks):
        output_file = f"{file_path[:-5]}-{i+1}.xlsx"
        chunk.to_excel(output_file, index=False)
        print(f"Chunk {i+1} saved to {output_file}")

if __name__ == "__main__":
    # Parse the command-line arguments
    parser = argparse.ArgumentParser(description="Chunk an Excel file")
    parser.add_argument("file", help="path to the Excel file")
    parser.add_argument("chunk_size", type=int, help="number of rows per chunk", nargs='?')
    args = parser.parse_args()
    
    # Check if both arguments are provided
    if not all(vars(args).values()):
        parser.print_help()
        sys.exit(1)
    
    # Chunk the Excel file
    chunk_excel_file(args.file, args.chunk_size)
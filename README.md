# Chunker

## Installation:
1. Install pipenv
```sh
pip install pipenv
```
2. Install depedencies
```sh
pipenv install
```

## Usage:
1. Use pipenv venv
```sh
pipenv shell
```
2. Run the script
```sh
python ./src/chunker.py /path/to/file  chunksize
```
e.g.
```sh
python ./src/chunker.py /home/user/Downloads/excel-file.xlsx 1000
```